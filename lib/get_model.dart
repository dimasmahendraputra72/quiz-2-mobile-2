import 'package:http/http.dart' as http;
import 'dart:convert';

class UserGet {
  String id;
  String firstName;
  String lastName;
  String email;
  String avatar;

  UserGet({this.id, this.firstName, this.lastName, this.email, this.avatar});

  factory UserGet.createUserGet(Map<String, dynamic> object) {
    return UserGet(
      id: object['id'].toString(),
      firstName: object['first_name'],
      lastName: object['last_name'],
      email: object['email'],
      avatar: object['avatar'],
    );
  }

  static Future<UserGet> connectToApiUser(String id) async {
    String apiURLPOST = 'https://reqres.in/api/users/' + id;

    var apiResult = await http.get(apiURLPOST);

    var jsonObject = json.decode(apiResult.body);

    var userData = (jsonObject as Map<String, dynamic>)['data'];

    return UserGet.createUserGet(userData);
  }
}
