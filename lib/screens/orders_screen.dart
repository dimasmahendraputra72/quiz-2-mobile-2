import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/orders.dart' show Orders;

import '../widgets/order_item.dart';
import '../widgets/app_drawer.dart';

class OrdersScreen extends StatelessWidget {
  static const routeName = '/About';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('About Aplikasi'),
        ),
        drawer: AppDrawer(),
        body: Padding(
          padding: EdgeInsets.symmetric(vertical: 250),
          child: Center(
            child: Column(
              children: [
                Text('Copyright By Dimas Mahendra Putra'),
                Text('18282004'),
              ],
            ),
          ),
        ));
  }
}
