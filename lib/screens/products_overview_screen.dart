import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../widgets/products_grid.dart';
import '../widgets/badge.dart';
import '../widgets/app_drawer.dart';
import '../providers/cart.dart';
import '../providers/products.dart';
import '../screens/cart_screen.dart';

enum FilterOptions {
  Favorites,
  All,
}

class ProductsOverviewScreen extends StatefulWidget {
  @override
  _ProductsOverviewScreenState createState() => _ProductsOverviewScreenState();
}

class _ProductsOverviewScreenState extends State<ProductsOverviewScreen> {
  var _showOnlyFavorites = false;
  var _isInit = true;
  var _isLoading = false;
  var motorimage2 =
      'https://d2pa5gi5n2e1an.cloudfront.net/webp/global/images/product/motorcycle/Yamaha_NMAX_2018/Yamaha_NMAX_2018_L_1.jpg';
  var motorimage3 =
      'https://image.cermati.com/c_fit,fl_progressive,h_240,q_80,w_360/av1q4aapcao0f1njyrtd.webp';
  var motorimage4 =
      'https://asset.kompas.com/crops/6M8uvx9GV815q1P4JiJ5iokX2nU=/9x132:5016x3470/750x500/data/photo/2021/01/09/5ff8b32ea2b13.jpg';
  var motorimage5 =
      'https://imgx.gridoto.com/crop/51x54:699x493/700x465/photo/2019/09/27/1139848128.jpg';
  var motorimage6 =
      'https://d2pa5gi5n2e1an.cloudfront.net/webp/global/images/product/motorcycle/Yamaha_AEROX_155_VVA/Yamaha_AEROX_155_VVA_L_1.jpg';
  var motorimage7 =
      'https://mk0hondacengkartgshx.kinstacdn.com/wp-content/uploads/2018/11/pcx160-majestic-matte-red010221-4-05022021-090329.jpg';
  var iconYellow = Color(0xFFf4bf47);
  @override
  void didChangeDependencies() {
    // if (_isInit) {
    //   setState(() {
    //     _isLoading = true;
    //   });
    //   Provider.of<Products>(context, listen: false)
    //       .fetchAndSetProducts()
    //       .then((_) {
    //     setState(() {
    //       _isLoading = false;
    //     });
    //   });
    // }
    // _isInit = false;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Showroom Otomotif Motor'),
          actions: <Widget>[
            PopupMenuButton(
              onSelected: (FilterOptions selectedValue) {
                setState(() {
                  if (selectedValue == FilterOptions.Favorites) {
                    _showOnlyFavorites = true;
                  } else {
                    _showOnlyFavorites = false;
                  }
                });
              },
              icon: Icon(Icons.more_vert),
              itemBuilder: (_) => [
                PopupMenuItem(
                    child: Text('Only Favorites'),
                    value: FilterOptions.Favorites),
                PopupMenuItem(child: Text('Show All'), value: FilterOptions.All)
              ],
            ),
            Consumer<Cart>(
              builder: (_, cart, child) => Badge(
                child: child,
                value: cart.itemCount.toString(),
              ),
              child: IconButton(
                icon: Icon(Icons.shopping_cart),
                onPressed: () {
                  Navigator.of(context).pushNamed(CartScreen.routeName);
                },
              ),
            ),
          ],
        ),
        drawer: AppDrawer(),
        body: SafeArea(
            child: Container(
          child: _food(),
        ))
        // body: _isLoading
        //     ? Center(
        //         child: CircularProgressIndicator(),
        //       )
        //     : ProductsGrid(_showOnlyFavorites),
        );
  }

  Widget _food() {
    return Padding(
        padding: const EdgeInsets.all(20),
        child: ListView(
          children: [
            Column(
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      height: 100.0,
                      width: 100.0,
                      child: Image.network(
                        motorimage2,
                        fit: BoxFit.cover,
                      ),
                    ),
                    SizedBox(
                      width: 16.0,
                    ),
                    Container(
                      child: Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              decoration: BoxDecoration(
                                  color: iconYellow,
                                  borderRadius: BorderRadius.circular(4.0)),
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 8.0, vertical: 4.0),
                                child: Row(
                                  children: <Widget>[
                                    Icon(
                                      Icons.star,
                                      size: 15.0,
                                    ),
                                    Text('4.5')
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 8.0,
                            ),
                            Text(
                              'Yamaha Nmax',
                              style: TextStyle(fontWeight: FontWeight.w600),
                            ),
                            Container(
                                width: 200.0,
                                child: Column(
                                  children: [
                                    Text(
                                      'Harga',
                                      style: TextStyle(color: Colors.grey),
                                    ),
                                    Text(
                                      'Rp. 32.000.000.00',
                                      style: TextStyle(color: Colors.grey),
                                    ),
                                  ],
                                ))
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      height: 100.0,
                      width: 100.0,
                      child: Image.network(
                        motorimage3,
                        fit: BoxFit.cover,
                      ),
                    ),
                    SizedBox(
                      width: 16.0,
                    ),
                    Container(
                      child: Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              decoration: BoxDecoration(
                                  color: iconYellow,
                                  borderRadius: BorderRadius.circular(4.0)),
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 8.0, vertical: 4.0),
                                child: Row(
                                  children: <Widget>[
                                    Icon(
                                      Icons.star,
                                      size: 15.0,
                                    ),
                                    Text('4.5')
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 8.0,
                            ),
                            Text(
                              'Yamaha R15',
                              style: TextStyle(fontWeight: FontWeight.w600),
                            ),
                            Container(
                                width: 200.0,
                                child: Column(
                                  children: [
                                    Text(
                                      'Harga',
                                      style: TextStyle(color: Colors.grey),
                                    ),
                                    Text(
                                      'Rp. 35.550.000.00',
                                      style: TextStyle(color: Colors.grey),
                                    ),
                                  ],
                                )),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      height: 100.0,
                      width: 100.0,
                      child: Image.network(
                        motorimage4,
                        fit: BoxFit.cover,
                      ),
                    ),
                    SizedBox(
                      width: 16.0,
                    ),
                    Container(
                      child: Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              decoration: BoxDecoration(
                                  color: iconYellow,
                                  borderRadius: BorderRadius.circular(4.0)),
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 8.0, vertical: 4.0),
                                child: Row(
                                  children: <Widget>[
                                    Icon(
                                      Icons.star,
                                      size: 15.0,
                                    ),
                                    Text('4.5')
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 8.0,
                            ),
                            Text(
                              'Kawasaki KLX150',
                              style: TextStyle(fontWeight: FontWeight.w600),
                            ),
                            Container(
                                width: 200.0,
                                child: Column(
                                  children: [
                                    Text(
                                      'Harga',
                                      style: TextStyle(color: Colors.grey),
                                    ),
                                    Text(
                                      'Rp. 39.250.000.00',
                                      style: TextStyle(color: Colors.grey),
                                    ),
                                  ],
                                )),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      height: 100.0,
                      width: 100.0,
                      child: Image.network(
                        motorimage5,
                        fit: BoxFit.cover,
                      ),
                    ),
                    SizedBox(
                      width: 16.0,
                    ),
                    Container(
                      child: Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              decoration: BoxDecoration(
                                  color: iconYellow,
                                  borderRadius: BorderRadius.circular(4.0)),
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 8.0, vertical: 4.0),
                                child: Row(
                                  children: <Widget>[
                                    Icon(
                                      Icons.star,
                                      size: 15.0,
                                    ),
                                    Text('4.5')
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 8.0,
                            ),
                            Text(
                              'Honda Scoopy cbs',
                              style: TextStyle(fontWeight: FontWeight.w600),
                            ),
                            Container(
                                width: 200.0,
                                child: Column(
                                  children: [
                                    Text(
                                      'Harga',
                                      style: TextStyle(color: Colors.grey),
                                    ),
                                    Text(
                                      'Rp. 25.550.000.00',
                                      style: TextStyle(color: Colors.grey),
                                    ),
                                  ],
                                )),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      height: 100.0,
                      width: 100.0,
                      child: Image.network(
                        motorimage6,
                        fit: BoxFit.cover,
                      ),
                    ),
                    SizedBox(
                      width: 16.0,
                    ),
                    Container(
                      child: Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              decoration: BoxDecoration(
                                  color: iconYellow,
                                  borderRadius: BorderRadius.circular(4.0)),
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 8.0, vertical: 4.0),
                                child: Row(
                                  children: <Widget>[
                                    Icon(
                                      Icons.star,
                                      size: 15.0,
                                    ),
                                    Text('4.5')
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 8.0,
                            ),
                            Text(
                              'Yamaha AEROX 155',
                              style: TextStyle(fontWeight: FontWeight.w600),
                            ),
                            Container(
                                width: 200.0,
                                child: Column(
                                  children: [
                                    Text(
                                      'Harga',
                                      style: TextStyle(color: Colors.grey),
                                    ),
                                    Text(
                                      'Rp. 37.550.000.00',
                                      style: TextStyle(color: Colors.grey),
                                    ),
                                  ],
                                )),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      height: 100.0,
                      width: 100.0,
                      child: Image.network(
                        motorimage7,
                        fit: BoxFit.cover,
                      ),
                    ),
                    SizedBox(
                      width: 16.0,
                    ),
                    Container(
                      child: Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              decoration: BoxDecoration(
                                  color: iconYellow,
                                  borderRadius: BorderRadius.circular(4.0)),
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 8.0, vertical: 4.0),
                                child: Row(
                                  children: <Widget>[
                                    Icon(
                                      Icons.star,
                                      size: 15.0,
                                    ),
                                    Text('4.5')
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 8.0,
                            ),
                            Text(
                              'Honda PCX 160',
                              style: TextStyle(fontWeight: FontWeight.w600),
                            ),
                            Container(
                                width: 200.0,
                                child: Column(
                                  children: [
                                    Text(
                                      'Harga',
                                      style: TextStyle(color: Colors.grey),
                                    ),
                                    Text(
                                      'Rp. 40.550.000.00',
                                      style: TextStyle(color: Colors.grey),
                                    ),
                                  ],
                                )),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
              ],
            ),
          ],
        ));
  }
}
